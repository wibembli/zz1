#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include"game.h"
#include"sdl12.h"

bool canmove(int newX, int newY, int currentplayer) {
 // Vérifier si les nouvelles coordonnées sont en dehors des limites du plateau
 if (newX < 0 || newX >= board_size || newY < 0 || newY >= board_size) {
 return false;
 }

 // Vérifier les conditions spécifiques au joueur
 if (currentplayer == 0) { // Joueur 1
 if (newX < 0 || newX >= board_size) {
 return false;
 }
 } else if (currentplayer == 1) { // Joueur 2
 if (newX < 0 || newX >= board_size) {
 return false;
 }
 }

 return true; // Le déplacement est possible
}
bool movePiece(Piece* board[board_size][board_size],Piece* piece, int newX, int newY,int currentplayer){
 int oldX = (piece->x -90)/CELL_SIZE;
 int oldY = (piece->y -90)/CELL_SIZE;
 int deltaX = abs(newX - oldX);
 int deltaY = abs(newY - oldY);
 int moveDistance = deltaX + deltaY;
 if ((moveDistance == piece->power)&&(canmove(newX, newY, currentplayer))){
    piece->x=90+newX*CELL_SIZE;
    piece->y=90+newY*CELL_SIZE;
Piece* temp=piece;
board[oldX][oldY]=board[newX][newY];
board[newX][newY]=piece ; 
board[oldX][oldY]->x=90+oldX*CELL_SIZE;
board[oldX][oldY]->y=90+oldY*CELL_SIZE;
 return true;
 }
 return false;
}
bool game_over(Piece P,int currentplayer)
{
 if(currentplayer==0)
 {
 if(P.x==board_size){return true;}
 }
 else if(currentplayer==1)
 {
 if(P.x==-1){return true;}
 }
 return false ;
}