#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include"game.h"
#include"sdl12.h"


#define CELL_SIZE 70
#define BOARD_SIZE 6
SDL_Texture* load_texture(const char* file, SDL_Renderer* renderer) {
 SDL_Texture* texture = IMG_LoadTexture(renderer, file);
 if (!texture) {
 printf("Erreur de chargement de la texture %s: %s\n", file, SDL_GetError());
 }
 return texture;
}

void initialize_board(Piece* board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer) {
 for (int i = 0; i < BOARD_SIZE; i++) {
 for (int j = 0; j < BOARD_SIZE; j++) {
 board[i][j] = (Piece*)malloc(sizeof(Piece));
 if (board[i][j]) {
 board[i][j]->texture = NULL;
 board[i][j]->width = CELL_SIZE;
 board[i][j]->height = CELL_SIZE;
 board[i][j]->x = 90 + j * CELL_SIZE;
 board[i][j]->y = 90 + i * CELL_SIZE;
 board[i][j]->power = -1;
 }
 }
 }

 SDL_Texture* piece_h1_1 = load_texture("/home/wibembli/shared/overnight/officiel/pign3.png", renderer);
 SDL_Texture* piece_h2_1 = load_texture("/home/wibembli/shared/overnight/officiel/pign2.png", renderer);
 SDL_Texture* piece_h3_1 = load_texture("/home/wibembli/shared/overnight/officiel/pign1.png", renderer);

 if (!piece_h1_1 || !piece_h2_1 || !piece_h3_1) {
 exit(1);
 }

 // Initialiser les pièces
 Piece* pieceh1_1 = (Piece*)malloc(sizeof(Piece));
 pieceh1_1->texture = piece_h1_1;
 pieceh1_1->width = CELL_SIZE;
 pieceh1_1 ->height = CELL_SIZE;
 pieceh1_1 ->x=90;
 pieceh1_1 ->y=90;
 pieceh1_1 ->power=1;
 board[0][0] = pieceh1_1;

 Piece* pieceh1_2 = (Piece*)malloc(sizeof(Piece));
 pieceh1_2->texture = piece_h1_1;
 pieceh1_2->width = CELL_SIZE;
 pieceh1_2->height = CELL_SIZE;
 pieceh1_2->y=90;
 pieceh1_2->x=440;
 pieceh1_2->power=1;
 board[5][0] = pieceh1_2;
 
 Piece* pieceh1_3 = (Piece*)malloc(sizeof(Piece));
 pieceh1_3->texture = piece_h1_1;
 pieceh1_3->width = CELL_SIZE;
 pieceh1_3->height = CELL_SIZE;
 pieceh1_3->x=90;
 pieceh1_3->y=440;
 pieceh1_3->power=1;
 board[0][5] = pieceh1_3;

 Piece* pieceh1_4 = (Piece*)malloc(sizeof(Piece));
 pieceh1_4->texture = piece_h1_1;
 pieceh1_4->width = CELL_SIZE;
 pieceh1_4->height = CELL_SIZE;
 pieceh1_4->x=440;
 pieceh1_4->y=440;
 pieceh1_4->power=1;
 board[5][5] = pieceh1_4;
 //
 Piece* pieceh2_1 = (Piece*)malloc(sizeof(Piece));
 pieceh2_1->texture = piece_h2_1;
 pieceh2_1->width = CELL_SIZE;
 pieceh2_1->height = CELL_SIZE;
 pieceh2_1->y=90;
 pieceh2_1->x=160;
 pieceh2_1->power=2;
 board[1][0] = pieceh2_1;

 Piece* pieceh2_2 = (Piece*)malloc(sizeof(Piece));
 pieceh2_2 ->texture = piece_h2_1;
 pieceh2_2 ->width = CELL_SIZE;
 pieceh2_2 ->height = CELL_SIZE;
 pieceh2_2 ->y=90;
 pieceh2_2 ->x=370;
 pieceh2_2 ->power=2;
 board[4][0] = pieceh2_2;

 Piece* pieceh2_3 = (Piece*)malloc(sizeof(Piece));
 pieceh2_3->texture = piece_h2_1;
 pieceh2_3->width = CELL_SIZE;
 pieceh2_3->height = CELL_SIZE;
 pieceh2_3->x=160;
 pieceh2_3->y=440;
 pieceh2_3->power=2;
 board[1][5] = pieceh2_3;

 Piece* pieceh2_4 = (Piece*)malloc(sizeof(Piece));
 pieceh2_4->texture = piece_h2_1;
 pieceh2_4->width = CELL_SIZE;
 pieceh2_4->height = CELL_SIZE;
 pieceh2_4->x=370;
 pieceh2_4->y=440;
 pieceh2_4->power=2;
 board[4][5] = pieceh2_4;
 
 Piece* pieceh3_1 = (Piece*)malloc(sizeof(Piece));
 pieceh3_1->texture = piece_h3_1;
 pieceh3_1->width = CELL_SIZE;
 pieceh3_1->height = CELL_SIZE;
 pieceh3_1->x=230;
 pieceh3_1->y=90;
 pieceh3_1->power=3;
 board[2][0] = pieceh3_1;

 Piece* pieceh3_2 = (Piece*)malloc(sizeof(Piece));
 pieceh3_2->texture = piece_h3_1;
 pieceh3_2->width = CELL_SIZE;
 pieceh3_2->height = CELL_SIZE;{
 
}
 pieceh3_2->x=300;
 pieceh3_2->y=90;
 pieceh3_2->power=3;
 board[3][0] = pieceh3_2;

 Piece* pieceh3_3 = (Piece*)malloc(sizeof(Piece));
 pieceh3_3->texture = piece_h3_1;
 pieceh3_3->width = CELL_SIZE;
 pieceh3_3->height = CELL_SIZE;
 pieceh3_3->x=230;
 pieceh3_3->y=440;
 pieceh3_3->power=3;
 board[2][5] = pieceh3_3;

 Piece* pieceh3_4 = (Piece*)malloc(sizeof(Piece));
 pieceh3_4->texture = piece_h3_1;
 pieceh3_4->width = CELL_SIZE;
 pieceh3_4->height = CELL_SIZE;
 pieceh3_4->x=300;
 pieceh3_4->y=440;
 pieceh3_4->power=3;
 board[3][5] = pieceh3_4;

}

void draw_board(Piece* board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer) {
 for (int i = 0; i < BOARD_SIZE; i++) {
 for (int j = 0; j < BOARD_SIZE; j++) {
 Piece* piece = board[i][j];
 if (piece && piece->texture) {
 SDL_Rect rect = { piece->x, piece->y, piece->width, piece->height };
 SDL_RenderCopy(renderer, piece->texture, NULL, &rect);
 }
 }
 }
}


bool handle_mouse_click(Piece* board[board_size][board_size], int mouse_x, int mouse_y, Piece** selected_piece, int currentplayer) {
 
 if (mouse_x >= 90 && mouse_x <= 540 && mouse_y >= 90 && mouse_y <= 540 ) {// il ya le probleme que le joueur peut cliquer pour sortir 
 int col = (mouse_x - 90) / CELL_SIZE;
 int row = (mouse_y - 90) / CELL_SIZE;
 if (*selected_piece == NULL) {
 // Aucune pièce sélectionnée, donc sélectionner une pièce si elle existe à cette position
 if(board[col][row] != NULL && board[col][row]->power != -1) {
 *selected_piece = board[col][row];
 return true;
 }
 }
 else{ 
 bool moveSuccess = movePiece(board, *selected_piece, col, row, currentplayer);
 if (moveSuccess) {

 selected_piece=NULL; // Réinitialiser la sélection après le déplacement
 return true; // Une pièce est déjà sélectionnée
 }
 }
 }
 return false;
}


int main()
{
 {
 if (SDL_Init(SDL_INIT_VIDEO) != 0) {
 fprintf(stderr, "Error: SDL_Init VIDEO (%s)\n", SDL_GetError());
 return EXIT_FAILURE;
 }
 if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
 fprintf(stderr, "Error: IMG_Init PNG (%s)\n", SDL_GetError());
 SDL_Quit();
 return EXIT_FAILURE;
 }

 SDL_Window* win = SDL_CreateWindow("Gyges", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
 600, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
 if (!win) {
 fprintf(stderr, "Error: SDL_CreateWindow (%s)\n", SDL_GetError());
 IMG_Quit();
 SDL_Quit();
 return EXIT_FAILURE;
 }

 SDL_Renderer* ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
 if (!ren) {
 fprintf(stderr, "Error: SDL_CreateRenderer (%s)\n", SDL_GetError());
 SDL_DestroyWindow(win);
 IMG_Quit();
 SDL_Quit();
 return EXIT_FAILURE;
 }

 SDL_Texture* background = load_texture("/home/wibembli/shared/overnight/officiel/gyges-board.png", ren);
 if (!background) {
 exit(1);
 }
 Piece* board[BOARD_SIZE][BOARD_SIZE];
 initialize_board(board, ren);
 bool running = true;
 Piece* selected_piece = NULL;
 int currentplayer=0;
 int i=0;
 while (running) {
 SDL_Event event;
 while (SDL_PollEvent(&event)) {
 if (event.type == SDL_QUIT) {
 running = false;
 } else if (event.type == SDL_MOUSEBUTTONDOWN) {
 if (event.button.button == SDL_BUTTON_LEFT) {
 int x = event.button.x;
 int y = event.button.y;
 printf("%d,%d ",x,y);
 if(handle_mouse_click(board, x, y, &selected_piece,currentplayer))
 {i++;
 }
 }
 if(i%2==0){currentplayer=1-currentplayer ;}
 }
 }
 SDL_RenderClear(ren);
 SDL_RenderCopy(ren, background, NULL, NULL);
 draw_board(board, ren);
 SDL_RenderPresent(ren);
 if (selected_piece != NULL && selected_piece->power == -1) {
 selected_piece = NULL;
 }
 }

 SDL_DestroyTexture(background);
 SDL_DestroyRenderer(ren);
 SDL_DestroyWindow(win);
 IMG_Quit();
 SDL_Quit();
 }
 return EXIT_SUCCESS;
}