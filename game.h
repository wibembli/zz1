#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define board_size 6 
typedef struct piece {
 SDL_Texture* texture; // Texture de la pièce
 int width;
 int height;
 int x; // Position x de la pièce
 int y; // Position y de la pièce
 int power ;
}Piece ;

typedef struct {
 Piece* board[board_size][board_size];
} Board;

typedef struct list {
 int x, y;
 struct list *next;
} list ; 

bool canmove(int newX, int newY, int currentplayer); 
bool movePiece(Piece* board[board_size][board_size],Piece* piece, int newX, int newY,int currentplayer);
bool game_over(Piece P,int currentplayer);
//list *plus_proche(Piece* board[board_size][board_size],list *l,int currentplayer);
//int length(list* l);
//void listDisplay(list * s);

#endif // GAME_H